package com.example.zeus.facerecognition;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    public TextView faceCountView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InputStream stream = getResources().openRawResource(R.raw.aaaa);
        Bitmap bitmap = BitmapFactory.decodeStream(stream);

        FaceDetector detector = new FaceDetector.Builder(getApplicationContext())
                .setTrackingEnabled(false)
                .build();

        // Create a frame from the bitmap and run face detection on the frame.
        Frame frame = new Frame.Builder().setBitmap(bitmap).build();
        SparseArray<Face> faces = detector.detect(frame);

        TextView faceCountView = (TextView) findViewById(R.id.face_count);
        faceCountView.setText(faces.size() + " faces detected");

        detector.release();
        if (!detector.isOperational()) {
            Toast.makeText(this, "Face detector dependencies are not yet available.", Toast.LENGTH_SHORT).show();
        }

        CustomView overlay = (CustomView) findViewById(R.id.customView);
        overlay.setContent(bitmap, faces);
        detector.release();


    }



}
